/*
 * DesertTortoiseModel.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.turtles.client;

import com.google.common.collect.ImmutableList;
import com.turtles.common.DesertTortoiseEntity;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

import javax.annotation.Nonnull;

public class DesertTortoiseModel<T extends DesertTortoiseEntity> extends SegmentedModel<T> {

    public ModelRenderer carapace;
    public ModelRenderer head;
    public ModelRenderer leg1;
    public ModelRenderer leg2;
    public ModelRenderer leg3;
    public ModelRenderer leg4;
    public ModelRenderer plastron;
    public ModelRenderer tail;

    public DesertTortoiseModel() {
        float yPos = 22F;

        carapace = new ModelRenderer(this, 0, 23);
        carapace.addBox(-3F, 0F, -3F, 6, 3, 6);
        carapace.setRotationPoint(0F, yPos - 4, 0F);

        head = new ModelRenderer(this, 0, 0);
        head.addBox(-2F, 0F, -4F, 4, 3, 4);
        head.setRotationPoint(0F, yPos - 3, -4F);

        leg1 = new ModelRenderer(this, 60, 0);
        leg1.addBox(0F, 0F, 0F, 1, 3, 1);
        leg1.setRotationPoint(2F, yPos, -3F);
        leg1.rotateAngleZ = 5.497787143782138F;

        leg2 = new ModelRenderer(this, 60, 0);
        leg2.addBox(0F, 0F, 0F, 1, 3, 1);
        leg2.setRotationPoint(2F, yPos, 2F);
        leg2.rotateAngleZ = 5.497787143782138F;

        leg3 = new ModelRenderer(this, 60, 0);
        leg3.addBox(-1F, 0F, 0F, 1, 3, 1);
        leg3.setRotationPoint(-2F, yPos, -3F);
        leg3.rotateAngleZ = 0.7853981633974483F;

        leg4 = new ModelRenderer(this, 60, 0);
        leg4.addBox(-1F, 0F, 0F, 1, 3, 1);
        leg4.setRotationPoint(-2F, yPos, 2F);
        leg4.rotateAngleZ = 0.7853981633974483F;

        plastron = new ModelRenderer(this, 24, 23);
        plastron.addBox(-4F, -1F, -4F, 8, 1, 8);
        plastron.setRotationPoint(0F, yPos, 0F);

        tail = new ModelRenderer(this, 58, 29);
        tail.addBox(0F, 0F, 0F, 1, 1, 2);
        tail.setRotationPoint(-0.5F, yPos - 1, 4F);
        tail.rotateAngleX = 5.934119456780721F;
    }

    @Override
    public void setRotationAngles(@Nonnull T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        head.rotateAngleX = headPitch / 57.29578F;
        head.rotateAngleY = netHeadYaw / 57.29578F;

        leg1.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        leg2.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        leg3.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        leg4.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;

        tail.rotateAngleY = MathHelper.cos(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
    }

    @Override
    @Nonnull
    public Iterable<ModelRenderer> getParts() {
        return ImmutableList.of(carapace, head, leg1, leg2, leg3, leg4, plastron, tail);
    }

}
