/*
 * MudTurtleRenderer.java
 *
 *  Copyright (c) 2019 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.turtles.client;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.turtles.common.MudTurtleEntity;
import com.turtles.common.TurtleMod;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;

public class MudTurtleRenderer<T extends MudTurtleEntity> extends MobRenderer<T, MudTurtleModel<T>> {
    private static final ResourceLocation SKIN = new ResourceLocation(TurtleMod.MODID, "textures/entity/turtles/mudturtle.png");

    public MudTurtleRenderer(EntityRendererManager rm) {
        super(rm, new MudTurtleModel<>(), 0.0f);
    }

    @Override
    protected void preRenderCallback(T entity, MatrixStack matrixStack, float unknown) {
        float scaleFactor = entity.getScaleFactor();
        matrixStack.scale(scaleFactor, scaleFactor, scaleFactor);
    }

    @Override
    @Nonnull
    public ResourceLocation getEntityTexture(@Nonnull T t) {
        return SKIN;
    }
}
