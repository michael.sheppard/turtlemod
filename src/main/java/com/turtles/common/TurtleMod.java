/*
 * turtles.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.turtles.common;

import com.turtles.client.DesertTortoiseRenderer;
import com.turtles.client.LittleTurtleRenderer;
import com.turtles.client.MudTurtleRenderer;
import com.turtles.client.TortoiseRenderer;
import net.minecraft.entity.*;
import net.minecraft.item.Food;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;

import java.util.*;

import net.minecraftforge.common.BiomeDictionary.Type;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistryEntry;
import net.minecraftforge.registries.ObjectHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@SuppressWarnings("unused")
@Mod(TurtleMod.MODID)
public class TurtleMod {

    public static final String MODID = "turtlemod";
    public static final String TURTLES = "turtles";

    private static final Logger LOGGER = LogManager.getLogger(TurtleMod.MODID);

    // List of always excluded biome types
    private static final List<Type> excludedBiomeTypes = new ArrayList<>(Arrays.asList(
            Type.END,
            Type.NETHER,
            Type.VOID,
            Type.COLD,
            Type.OCEAN,
            Type.CONIFEROUS,
            Type.MOUNTAIN,
            Type.MUSHROOM,
            Type.SNOWY
    ));

    public static final String TURTLE_COOKED_NAME = "cooked_turtlemeat";
    public static final String TURTLE_RAW_NAME = "turtlemeat";
    public static final String TURTLE_LEATHER_NAME = "turtle_leather";

    public static final String TORTOISE_NAME = "tortoise";
    public static final String DESERT_TORTOISE_NAME = "deserttortoise";
    public static final String LITTLE_TURTLE_NAME = "littleturtle";
    public static final String MUD_TURTLE_NAME = "mudturtle";

    public static final String TORTOISE_SPAWN_EGG = "tortoise_spawn_egg";
    public static final String DESERT_TORTOISE_SPAWN_EGG = "desert_tortoise_spawn_egg";
    public static final String LITTLE_TURTLE_SPAWN_EGG = "little_turtle_spawn_egg";
    public static final String MUD_TURTLE_SPAWN_EGG = "mudturtle_spawn_egg";

    public static final String TURTLE_LOOT = "turtle_loot";

    public TurtleMod() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::initClient);

        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, ConfigHandler.commonSpec);

        MinecraftForge.EVENT_BUS.register(this);
    }

    public void setup(final FMLCommonSetupEvent event) {
        ConfigHandler.loadConfig();

        registerSpawns();
        MinecraftForge.EVENT_BUS.register(new SpawnCheck());
    }

    public void initClient(final FMLClientSetupEvent event) {
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.DESERT_TORTOISE, DesertTortoiseRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.LITTLE_TURTLE, LittleTurtleRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.TORTOISE, TortoiseRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.MUD_TURTLE, MudTurtleRenderer::new);
    }

    @Mod.EventBusSubscriber(modid=TurtleMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
    @ObjectHolder(TurtleMod.MODID)
    public static class RegistryEvents {
        public final static EntityType<DesertTortoiseEntity> DESERT_TORTOISE = EntityType.Builder.<DesertTortoiseEntity>create(DesertTortoiseEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);
        public final static EntityType<LittleTurtleEntity> LITTLE_TURTLE = EntityType.Builder.<LittleTurtleEntity>create(LittleTurtleEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);
        public final static EntityType<TortoiseEntity> TORTOISE = EntityType.Builder.<TortoiseEntity>create(TortoiseEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);
        public final static EntityType<MudTurtleEntity> MUD_TURTLE = EntityType.Builder.<MudTurtleEntity>create(MudTurtleEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);

        @SubscribeEvent
        public static void onEntityRegistry(final RegistryEvent.Register<EntityType<?>> event) {
            event.getRegistry().registerAll(
                    setup(DESERT_TORTOISE, DESERT_TORTOISE_NAME),
                    setup(LITTLE_TURTLE, LITTLE_TURTLE_NAME),
                    setup(TORTOISE, TORTOISE_NAME),
                    setup(MUD_TURTLE, MUD_TURTLE_NAME)
            );
        }

        @SubscribeEvent
        public static void onItemRegistry(final RegistryEvent.Register<Item> event) {
            event.getRegistry().registerAll(
                    setup(new TurtleMeatItem(new Item.Properties().group(ItemGroup.FOOD)
                            .food((new Food.Builder()).hunger(8).saturation(0.8f).meat().build())), TURTLE_COOKED_NAME),

                    setup(new TurtleMeatItem(new Item.Properties().group(ItemGroup.FOOD)
                            .food((new Food.Builder()).hunger(8).saturation(0.8f).meat().build())), TURTLE_RAW_NAME),

                    setup(new Item(new Item.Properties().group(ItemGroup.MATERIALS)), TURTLE_LEATHER_NAME),

                    setup(new SpawnEggItem(TORTOISE, 0x008B45, 0xC0FF3E, new Item.Properties().group(ItemGroup.MISC)), TORTOISE_SPAWN_EGG),
                    setup(new SpawnEggItem(LITTLE_TURTLE, 0xFF7F24, 0xFF8C69, new Item.Properties().group(ItemGroup.MISC)), LITTLE_TURTLE_SPAWN_EGG),
                    setup(new SpawnEggItem(DESERT_TORTOISE, 0x8B4513, 0x8B4C39, new Item.Properties().group(ItemGroup.MISC)), DESERT_TORTOISE_SPAWN_EGG),
                    setup(new SpawnEggItem(MUD_TURTLE, 0x8B4513, 0x8B4C39, new Item.Properties().group(ItemGroup.MISC)), MUD_TURTLE_SPAWN_EGG)
            );
        }

        public static <T extends IForgeRegistryEntry<T>> T setup(final T entry, final String name) {
            return setup(entry, new ResourceLocation(TurtleMod.MODID, name));
        }

        public static <T extends IForgeRegistryEntry<T>> T setup(final T entry, final ResourceLocation registryName) {
            entry.setRegistryName(registryName);
            return entry;
        }

    }

    private void registerSpawns() {
        Biome[] savannaBiomes = getBiomes(false, Type.SAVANNA);
        Biome[] desertBiomes = getBiomes(false, Type.HOT, Type.DRY, Type.SANDY);
        Biome[] combinedBiomes = getBiomes(true, Type.FOREST, Type.SAVANNA);
        Biome[] swampyBiomes = getBiomes(true, Type.SWAMP, Type.RIVER);

        int minSpawn = ConfigHandler.CommonConfig.getMinSpawn();
        int maxSpawn = ConfigHandler.CommonConfig.getMaxSpawn();

        registerEntitySpawn(RegistryEvents.DESERT_TORTOISE, desertBiomes, ConfigHandler.CommonConfig.getDesertTortoiseSpawnProb(), minSpawn, maxSpawn);
        registerEntitySpawn(RegistryEvents.LITTLE_TURTLE, combinedBiomes, ConfigHandler.CommonConfig.getLittleTurtleSpawnProb(), minSpawn, maxSpawn);
        registerEntitySpawn(RegistryEvents.TORTOISE, savannaBiomes, ConfigHandler.CommonConfig.getTortoiseSpawnProb(), minSpawn, maxSpawn);
        registerEntitySpawn(RegistryEvents.MUD_TURTLE, swampyBiomes, ConfigHandler.CommonConfig.getMudTurtleSpawnProb(), minSpawn, maxSpawn);
    }

    private Biome[] getBiomes(boolean orTypes, Type... types) {
        LinkedList<Biome> list = new LinkedList<>();
        Collection<Biome> biomes = ForgeRegistries.BIOMES.getValues();

        for (Biome biome : biomes) {
            Set<Type> bTypes = BiomeDictionary.getTypes(biome);

            // we exclude certain biomes, i.e., turtles are ectothermic, no cold biomes
            if (excludeThisBiome(bTypes)) {
                continue;
            }
            // process remaining biomes
            if (orTypes) { // add any biome that contains any of the listed types (logical OR)
                for (Type t : types) {
                    if (BiomeDictionary.hasType(biome, t)) {
                        if (!list.contains(biome)) {
                            getLogger().info("Adding " + biome.getRegistryName() + " biome for spawning");
                            list.add(biome);
                        }
                    }
                }
            } else { // add any biome that contains all the types listed (logical AND)
                int count = types.length;
                int shouldAdd = 0;
                for (Type t : types) {
                    if (BiomeDictionary.hasType(biome, t)) {
                        shouldAdd++;
                    }
                }
                if (!list.contains(biome) && shouldAdd == count) {
                    getLogger().info("Adding " + biome.getRegistryName() + " biome for spawning");
                    list.add(biome);
                }
            }
        }
        return list.toArray(new Biome[0]);
    }

    private boolean excludeThisBiome(Set<Type> types) {
        boolean excludeBiome = false;
        for (Type ex : excludedBiomeTypes) {
            if (types.contains(ex)) {
                excludeBiome = true;
                break;
            }
        }
        return excludeBiome;
    }

    private void registerEntitySpawn(EntityType<? extends LivingEntity> type, Biome[] biomes, int spawnProb, int minSpawn, int maxSpawn) {
        if (spawnProb <= 0) {
            // do not spawn this entity
            return;
        }

        for (Biome bgb : biomes) {
            Biome biome = ForgeRegistries.BIOMES.getValue(bgb.getRegistryName());
            if (biome != null) {
                biome.getSpawns(EntityClassification.CREATURE).add(new Biome.SpawnListEntry(type, spawnProb, minSpawn, maxSpawn));
            }
        }
    }

    public static Logger getLogger() {
        return LOGGER;
    }
}
