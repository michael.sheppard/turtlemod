/*
 * DesertTortoiseModel.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.turtles.common;

import net.minecraft.block.BlockState;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;

import javax.annotation.Nonnull;

public final class DesertTortoiseEntity extends CreatureEntity {

    private static final DataParameter<Float> HEALTH = EntityDataManager.createKey(DesertTortoiseEntity.class, DataSerializers.FLOAT);
    public EntitySize turtleSize = new EntitySize(1.0f, 0.3f, false);

    private final float WIDTH = 1.0f;
    private final float HEIGHT = 0.3f;
    private final float scaleFactor;

    public DesertTortoiseEntity(EntityType<? extends DesertTortoiseEntity> entity, World world) {
        super(entity, world);

        if (ConfigHandler.CommonConfig.useRandomScaling()) {
            float scale = rand.nextFloat();
            scaleFactor = scale < 0.6F ? 1.0F : scale;
        } else {
            scaleFactor = 1.0F;
        }
        turtleSize.scale(WIDTH * getScaleFactor(), HEIGHT * getScaleFactor());
    }

    @SuppressWarnings("unused")
    public DesertTortoiseEntity(World world) {
        this(TurtleMod.RegistryEvents.DESERT_TORTOISE, world);
    }

    public float getScaleFactor() {
        return scaleFactor;
    }

    @Override
    protected void registerGoals() {
        double moveSpeed = 0.75;
        goalSelector.addGoal(1, new SwimGoal(this));
        goalSelector.addGoal(2, new EatGrassGoal(this));
        goalSelector.addGoal(3, new RandomWalkingGoal(this, moveSpeed));
        goalSelector.addGoal(4, new LookAtGoal(this, PlayerEntity.class, 6.0F));
        goalSelector.addGoal(5, new LookRandomlyGoal(this));
    }

    @Override
    protected void registerAttributes() {
        super.registerAttributes();
        getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(8.0); // health
        getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.2); // move speed
    }

    @Override
    public void tick() {
        // kill in cold biomes
        Vec3d v = getPositionVec();
        BlockPos bp = new BlockPos(v.x, v.y, v.z);
        Biome biome = world.getBiome(bp);
        if (biome.getTemperature(bp) <= 0.25) {
            attackEntityFrom(DamageSource.STARVE, 4.0f);
        }
        super.tick();
    }

    @Override
    public boolean canDespawn(double distanceToPlayer) {
        return false;
    }

    @Override
    protected float getStandingEyeHeight(@Nonnull Pose pose, @Nonnull EntitySize size) {
        return HEIGHT * 0.9f;
    }

    @Nonnull
    @Override
    public EntityType<?> getType() {
        return TurtleMod.RegistryEvents.DESERT_TORTOISE;
    }

    @Nonnull
    @Override
    public EntitySize getSize(@Nonnull Pose p) {
        return new EntitySize(WIDTH, HEIGHT, false);
    }

    @Override
    protected void registerData() {
        super.registerData();
        dataManager.register(HEALTH, getHealth());
    }

    @Nonnull
    @Override
    public ResourceLocation getLootTable() {
        return new ResourceLocation(TurtleMod.MODID, TurtleMod.TURTLE_LOOT);
    }

    @Override
    protected int getExperiencePoints(@Nonnull PlayerEntity playerEntity) {
        return 1 + world.rand.nextInt(4);
    }

    @Override
    protected void updateAITasks() {
        dataManager.set(HEALTH, getHealth());
    }

    @Override
    protected void playStepSound(@Nonnull BlockPos pos, @Nonnull BlockState blockIn) {
        playSound(getStepSound(), 0.15F, 1.0F);
    }

    protected SoundEvent getStepSound() {
        return SoundEvents.ENTITY_CHICKEN_STEP;
    }

    @Override
    protected float getSoundVolume() {
        return 0.4F;
    }
}
