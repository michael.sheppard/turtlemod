/*
 * ConfigHandler.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.turtles.common;

import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import net.minecraftforge.common.ForgeConfigSpec;
import org.apache.commons.lang3.tuple.Pair;

import java.nio.file.Paths;

public class ConfigHandler {

    public static void loadConfig() {
        CommentedFileConfig.builder(Paths.get("config", TurtleMod.TURTLES, TurtleMod.MODID + ".toml")).build();
    }

    public static class CommonConfig {
        public static ForgeConfigSpec.IntValue desertTortoiseSpawnProb;
        public static ForgeConfigSpec.IntValue littleTurtleSpawnProb;
        public static ForgeConfigSpec.IntValue tortoiseSpawnProb;
        public static ForgeConfigSpec.IntValue mudTurtleSpawnProb;
        public static ForgeConfigSpec.IntValue minSpawn;
        public static ForgeConfigSpec.IntValue maxSpawn;
        public static ForgeConfigSpec.BooleanValue randomScale;

        public CommonConfig(ForgeConfigSpec.Builder builder) {
            builder.comment("Reptile Mod Config")
                   .push("CommonConfig");

            minSpawn = builder
                    .comment("Minimum number of reptiles to spawn at one time")
                    .translation("config.turtles.minSpawn")
                    .defineInRange("minSpawn", 1, 1, 5);

            maxSpawn = builder
                    .comment("Maximum number of turtles to spawn at one time")
                    .translation("config.turtles.maxSpawn")
                    .defineInRange("maxSpawn", 4, 1, 10);

            desertTortoiseSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.turtles.desertTortoiseSpawnProb")
                    .defineInRange("desertTortoiseSpawnProb", 10, 0, 100);

            littleTurtleSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.turtles.littleTurtleSpawnProb")
                    .defineInRange("littleTurtleSpawnProb", 10, 0, 100);

            tortoiseSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.turtles.tortoiseSpawnProb")
                    .defineInRange("tortoiseSpawnProb", 10, 0, 100);

            mudTurtleSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.turtles.mudTurtleSpawnProb")
                    .defineInRange("mudTurtleSpawnProb", 10, 0, 100);

            randomScale = builder
                    .comment("Set to false to disable random scaling of monitors, default is true.")
                    .translation("config.turtles.randomScale")
                    .define("randomScale", true);

            builder.pop();
        }

        public static boolean useRandomScaling() {
            return randomScale.get();
        }

        public static int getDesertTortoiseSpawnProb() {
            return desertTortoiseSpawnProb.get();
        }

        public static int getLittleTurtleSpawnProb() {
            return littleTurtleSpawnProb.get();
        }

        public static int getTortoiseSpawnProb() {
            return tortoiseSpawnProb.get();
        }

        public static int getMudTurtleSpawnProb() { return mudTurtleSpawnProb.get(); }

        public static int getMinSpawn() {
            return minSpawn.get();
        }

        public static int getMaxSpawn() {
            return maxSpawn.get();
        }

    }

    static final ForgeConfigSpec commonSpec;
    public static final CommonConfig COMMON_CONFIG;
    static {
        final Pair<CommonConfig, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(CommonConfig::new);
        commonSpec = specPair.getRight();
        COMMON_CONFIG = specPair.getLeft();
    }
}
